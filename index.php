<?php
header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
	session_start();
	$messages = array();

	if (!empty($_COOKIE['save'])) {
    setcookie('save', '', 100000);
    $messages[] = 'Спасибо, результаты сохранены.';
	if (!empty($_COOKIE['login']))
	{
		$messages[]='Вы можете выполнить вход с логином ' . $_COOKIE['login'] . ' и паролем ' . $_COOKIE['pass'];
		setcookie('login', '', 100000);
		setcookie('pass', '', 100000);
	}
  }

  $error = false;
  $errors = array();
  $errors['name'] = !empty($_COOKIE['name_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['year'] = !empty($_COOKIE['year_error']);
  $errors['gender'] = !empty($_COOKIE['gender_error']);
  $errors['limb'] = !empty($_COOKIE['limb_error']);
  $errors['specs'] = !empty($_COOKIE['specs_error']);
  $errors['check'] = !empty($_COOKIE['check_error']);

  if ($errors['name']) {
    $error = true;
    setcookie('name_error', '', 100000);
    $messages[] = '<div class="error">Заполните имя.</div>';
  }
 
 if ($errors['email']) {
    $error = true;
    setcookie('email_error', '', 100000);
    $messages[] = '<div class="error">Заполните почту.</div>';
  }
   if ($errors['year']) {
    $error = true;
    setcookie('year_error', '', 100000);
    $messages[] = '<div class="error">Заполните дату рождения.</div>';
  }
   if ($errors['gender']) {
    $error = true;
    setcookie('gender_error', '', 100000);
    $messages[] = '<div class="error">Заполните пол.</div>';
  }
   if ($errors['limb']) {
    $error = true;
    setcookie('limb_error', '', 100000);
    $messages[] = '<div class="error">Заполните число конечностей.</div>';
  }
   if ($errors['specs']) {
    $error = true;
    setcookie('specs_error', '', 100000);
    $messages[] = '<div class="error">Заполните способность.</div>';
  }
  
  if ($errors['check']) {
    $error = true;
    setcookie('check_error', '', 100000);
    $messages[] = '<div class="error">Необходимо согласие.</div>';
  }
  $values = array();
  
  $values['name'] = empty($_COOKIE['name_value']) ? '' : $_COOKIE['name_value'];
  $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
  $values['year'] = empty($_COOKIE['year_value']) ? '' : $_COOKIE['year_value'];
  $values['gender'] = empty($_COOKIE['gender_value']) ? '' : $_COOKIE['gender_value'];
  $values['limb'] = empty($_COOKIE['limb_value']) ? '' : $_COOKIE['limb_value'];
  $values['specs'] = empty($_COOKIE['specs_value']) ? '' : $_COOKIE['specs_value'];
  $values['specs'] = empty($_COOKIE['bio_value']) ? '' : $_COOKIE['bio_value'];
  $values['check'] = empty($_COOKIE['check_value']) ? '' : $_COOKIE['check_value'];
  
  if (!$error && !empty($_SESSION['login'])) {
	  $user = 'u20338';
	  $pass = '9055271';
	  $db = new PDO('mysql:host=localhost;dbname=u20338', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
	  $stmt = $db->prepare('SELECT name, email, birth_year, gender, limb_count, powers, bio FROM application WHERE username=:un'); 
	  $stmt->bindParam(':un', $_SESSION['login']);
	  $stmt->execute();
	  $row = $stmt->fetchAll(); 
	  $values['name'] = $row[0]['name'];
	  $values['email'] = $row[0]['email'];
	  $values['year'] = $row[0]['birth_year'];
	  $values['gender'] = $row[0]['gender'];
	  $values['limb'] = $row[0]['limb_count'];
	  $values['specs'] = $row[0]['powers'];
	  $values['bio'] = $row[0]['bio'];
  }

  
  include('form.php');
  
} else
{
	$testname='/^[А-ЯЁ][а-яё]*/';
	
$errors = FALSE;
if (!preg_match($testname, $_POST['name'])) {
  setcookie('name_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
} 
else {
    setcookie('name_value', $_POST['name'], time() + 30 * 24 * 60 * 60 * 12);
}

if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
  setcookie('email_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
    setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60 * 12);
}

if (empty($_POST['year'])) {
  setcookie('year_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
    setcookie('year_value', $_POST['year'], time() + 30 * 24 * 60 * 60 * 12);
}

if (empty($_POST['gender'])) {
  setcookie('gender_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
    setcookie('gender_value', $_POST['gender'], time() + 30 * 24 * 60 * 60 * 12);
}

if (empty($_POST['limb'])) {
  setcookie('limb_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
    setcookie('limb_value', $_POST['limb'], time() + 30 * 24 * 60 * 60 * 12);
}

if (empty($_POST['specs'])) {
  setcookie('specs_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
    setcookie('specs_value', $_POST['specs'], time() + 30 * 24 * 60 * 60 * 12);
}

if (empty($_POST['check'])) {
  setcookie('check_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
    setcookie('check_value', $_POST['check'], time() + 30 * 24 * 60 * 60 * 12);
}

setcookie('bio_value', $_POST['bio'], time() + 30 * 24 * 60 * 60 * 12);

if ($errors) {
  header('Location: index.php');
  exit();
}
else {

    setcookie('name_error', '', 100000);
	setcookie('email_error', '', 100000);
	setcookie('gender_error', '', 100000);
	setcookie('year_error', '', 100000);
	setcookie('limb_error', '', 100000);
	setcookie('specs_error', '', 100000);
	setcookie('check_error', '', 100000);

}

$user = 'u20338';
$pass = '9055271';
$db = new PDO('mysql:host=localhost;dbname=u20338', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

try {
	 if (session_start() && !empty($_SESSION['login'])) {
		$stmt = $db->prepare("UPDATE application SET name=:name, email=:email, birth_year=:birth_year, gender=:gender, limb_count=:limb_count, powers=:powers, bio=:bio WHERE username=:un");
		$stmt->bindParam(':name', $name);
		$stmt->bindParam(':email', $email);
		$stmt->bindParam(':birth_year', $birth_year);
		$stmt->bindParam(':gender', $gender);
		$stmt->bindParam(':limb_count', $limb_count);
		$stmt->bindParam(':powers', $powers);
		$stmt->bindParam(':bio', $bio);
		$stmt->bindParam(':un', $_SESSION['login']);
		
		$name=$_POST['name'];
		$email=$_POST['email'];
		$birth_year=$_POST['year'];
		$gender=$_POST['gender'];
		$limb_count=$_POST['limb'];
		$powers=$_POST['specs'];
		$bio=$_POST['bio'];
		
		$stmt->execute();
		
	}
	else
	{
		$stmt = $db->prepare("INSERT INTO application (name, email, birth_year, gender, limb_count, powers, bio, username, password) VALUES (:name, :email, :birth_year, :gender, :limb_count, :powers, :bio, :un, :ps)");
		$stmt->bindParam(':name', $name);
		$stmt->bindParam(':email', $email);
		$stmt->bindParam(':birth_year', $birth_year);
		$stmt->bindParam(':gender', $gender);
		$stmt->bindParam(':limb_count', $limb_count);
		$stmt->bindParam(':powers', $powers);
		$stmt->bindParam(':bio', $bio);
		$stmt->bindParam(':un', $username);
		$stmt->bindParam(':ps', $password);
		
		$name=$_POST['name'];
		$email=$_POST['email'];
		$birth_year=$_POST['year'];
		$gender=$_POST['gender'];
		$limb_count=$_POST['limb'];
		$powers=$_POST['specs'];
		$bio=$_POST['bio'];
		$username = 'p' . substr(md5(strtolower(str_replace(' ', '', $name)) . $email . $birth_year . $gender . $limb_count . $powers . $bio . strval(rand())), 0, 5);
		$password = substr(sha1($username), 0, 10);
		
		setcookie('login', $username, time() * 24);
		setcookie('pass', $password, time() * 24);
		
		$password = password_hash($password, PASSWORD_DEFAULT);		
		$stmt->execute();
	}
	
	setcookie('save', '1');

	header('Location: index.php');
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
}
}


