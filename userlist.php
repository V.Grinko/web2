<?php

$user = 'u20338';
$pass = '9055271';
$db = new PDO('mysql:host=localhost;dbname=u20338', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

if ($_SERVER['REQUEST_METHOD'] == 'GET')
{
	$stmt = $db->prepare('SELECT * FROM application');
	$stmt->execute();
	$data = $stmt->fetchAll();
?>

<link rel="stylesheet" type="text/css" href="style.css" />

		<form method="post" action="">
			<label for="user-name">Имя пользователя</label>
			<select id="user-name" name="user">
				<?php
					if (empty($data))
						print('<option>Пользователей нет</option>');
					else
						for($i = 0; $i < count($data); ++$i)
						{
							printf('<option>%s</option>', $data[$i]['username']);
						}
				?>
			</select>
			<?php
				if (!empty($data))
					print('<input type="submit" value="Показать данные выбранного пользователя" />');
			?>
		</form>
		<form action="login.php">
			<input type="submit" value="Выйти" />
		</form>

<?php
} else {
	$stmt=$db->prepare('SELECT * FROM application WHERE username=:un');
	$stmt->bindParam(':un', $_POST['user']);
	$stmt->execute();
	$data = $stmt->fetchAll();
	
	if (!empty($data))
	{
?>

<link rel="stylesheet" type="text/css" href="style.css" />

		<table class="table-bordered" style="margin: 10px;">
			<tr>
				<th>ИД</th>
				<th>Логин</th>
				<th>Имя</th>
				<th>Почта</th>
				<th>Годы рождения</th>
				<th>Пол</th>
				<th>Количество конечностей</th>
				<th>Сверхспособность</th>
				<th>Биография</th>
			</tr>
			<tr>
				<?php
					printf('<td>%s</td>', $data[0]['id']);
					printf('<td>%s</td>', $data[0]['username']);
					printf('<td>%s</td>', $data[0]['name']);
					printf('<td>%s</td>', $data[0]['email']);
					printf('<td>%s</td>', $data[0]['birth_year']);
					
					if ($data[0]['gender'] == 'male')
						printf('<td>%s</td>', 'Мужской');
					else
						printf('<td>%s</td>', 'Женский');
					
					printf('<td>%s</td>', $data[0]['limb_count']);
					
					if ($data[0]['powers'] == 'undead')
						printf('<td>%s</td>', 'Бессмертие');
					if ($data[0]['powers'] == 'wall-through')
						printf('<td>%s</td>', 'Проходить сквозь стены');
					if ($data[0]['powers'] == 'levitation')
						printf('<td>%s</td>', 'Левитация');
					
					printf('<td>%s</td>', $data[0]['bio']);
				?>
			</tr>
		</table>
		
		<form method="get" action="">
			<input type="submit" value="Назад" />
		</form>
		
		<form method="get" action="userdelete.php">
			<select name="username" class="invisible" selected="selected"><?php printf('<option>%s</option>', $_POST['user']); ?></select>
			<input type="submit" value="Удалить пользователя" />
		</form>
<?php
	}
}
?>