<!DOCTYPE html>
<html lang="ru">
   <head>
       <style>
/* Сообщения об ошибках и поля с ошибками выводим с красным бордюром. */
.error {
  border: 2px solid red;
}
    </style>
    <link rel="stylesheet" type="text/css" href="style.css" />
	  <meta charset="utf-8">
      <title>Форма</title>
	</head>
	<body class="bg-gray m-0">
	<?php
if (!empty($messages)) {
  print('<div id="messages">');
  // Выводим все сообщения.
  foreach ($messages as $message) {
    print($message);
  }
  print('</div>');
}

// Далее выводим форму отмечая элементы с ошибками классом error
// и задавая начальные значения элементов ранее сохраненными.
?>
	<div class="text-centered bg-white p-3 border-bottom">
		<h1>Форма</h1>
	</div>
	
	<div class="p-3" style="margin-left: 100px;">
	<div class="bg-white border round p-3" style="width: 200px">
	<form method="post" action="">
	<label>
        Имя:<br />
        <input name="name" <?php if ($errors['name']) {print 'class="error"';} ?> 
          value="<?php print $values['name']; ?>" />
      </label><br />

      <label>
        E-mail:<br />
        <input name="email" <?php if ($errors['email']) {print 'class="error"';} ?> 
          value="<?php print $values['email']; ?>"
          type="email" />
      </label><br />
	  
	  <label>
        Год рождения:<br />
        <select name="year"> <?php if ($errors['year']) {print 'class="error"';} ?> 
          <option value="2000-2010" <?php if ($values['year'] == '2000-2010') {print 'selected="selected"';} ?> >2000-2010</option>
          <option value="1990-1999" <?php if ($values['year'] == '1990-1999') {print 'selected="selected"';} ?>>1990-1999</option>
          <option value="1980-1989" <?php if ($values['year'] == '1980-1989') {print 'selected="selected"';} ?>>1980-1989</option>
		  <option value="1970-1979" <?php if ($values['year'] == '1970-1979') {print 'selected="selected"';} ?>>1970-1979</option>
        </select>
      </label><br />
	  
	  Пол:<br />
      <label><input type="radio" <?php if ($values['gender'] == 'woman') {print 'checked="checked"';} ?>
        name="gender" value="woman" />
        Ж</label>
      <label><input type="radio" <?php if ($values['gender'] == 'man') {print 'checked="checked"';} ?>
        name="gender" value="man" />
        М</label><br />
		
		Количество конечностей:<br />
      <label><input type="radio" <?php if ($values['limb'] == '2') {print 'checked="checked"';} ?>
        name="limb" value="2" />
        2</label>
      <label><input type="radio" <?php if ($values['limb'] == '3') {print 'checked="checked"';} ?>
        name="limb" value="3" />
        3</label>
		<label><input type="radio" <?php if ($values['limb'] == '4') {print 'checked="checked"';} ?>
        name="limb" value="4" />
        4</label><br />
		
		<label>
        Сверхспособности:
        <br />
        <select name="specs" <?php if ($errors['specs']) {print 'class="error"';} ?> 
          multiple="multiple">
          <option <?php if ($values['specs'] == 'undead') print ('selected="selected"'); ?> value="undead">Бессмертие</option>
          <option <?php if ($values['specs'] == 'wall-through') print ('selected="selected"'); ?> value="wall-through">Прохождение сквозь стены</option>
          <option <?php if ($values['specs'] == 'levitation') print ('selected="selected"'); ?> value="levitation">Левитация</option>
        </select>
      </label><br />
	  
	   <label>
        Биография:<br />
        <textarea name="bio"></textarea>
      </label><br />
	  
      <label><input type="checkbox" checked="checked" <?php if ($errors['check']) {print 'class="error"';} ?> 
        name="check" <?php if ($values['check']) { print 'value="checked"'; }?>/>
        C контрактом ознакомлен</label><br />
	</div>
	  
	   <input type="submit" style="margin: 20px 80px;" value="Отправить" />
	   </form>
		<form action="login.php" method="">
			<?php
				if (empty($_SESSION['login']))
					print('<input type="submit" value="Войти в систему" />');
				else
					print('<input type="submit" value="Выйти из ' . $_SESSION['login'] . '" />');
			?>
		</form>
	</div>
		
	  </body>
</html> 